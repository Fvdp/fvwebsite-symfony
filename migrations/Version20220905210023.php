<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220905210023 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE etape_guide ADD chapitre_guide_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE etape_guide ADD CONSTRAINT FK_7A04434B92E6C00F FOREIGN KEY (chapitre_guide_id) REFERENCES chapitre_guide (id)');
        $this->addSql('CREATE INDEX IDX_7A04434B92E6C00F ON etape_guide (chapitre_guide_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE etape_guide DROP FOREIGN KEY FK_7A04434B92E6C00F');
        $this->addSql('DROP INDEX IDX_7A04434B92E6C00F ON etape_guide');
        $this->addSql('ALTER TABLE etape_guide DROP chapitre_guide_id');
    }
}
