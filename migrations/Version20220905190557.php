<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220905190557 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE chapitre_guide ADD guide_id INT NOT NULL');
        $this->addSql('ALTER TABLE chapitre_guide ADD CONSTRAINT FK_7E006A54D7ED1D4B FOREIGN KEY (guide_id) REFERENCES guide (id)');
        $this->addSql('CREATE INDEX IDX_7E006A54D7ED1D4B ON chapitre_guide (guide_id)');
        $this->addSql('ALTER TABLE user ADD chapitre_guide_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64992E6C00F FOREIGN KEY (chapitre_guide_id) REFERENCES chapitre_guide (id)');
        $this->addSql('CREATE INDEX IDX_8D93D64992E6C00F ON user (chapitre_guide_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE chapitre_guide DROP FOREIGN KEY FK_7E006A54D7ED1D4B');
        $this->addSql('DROP INDEX IDX_7E006A54D7ED1D4B ON chapitre_guide');
        $this->addSql('ALTER TABLE chapitre_guide DROP guide_id');
        $this->addSql('ALTER TABLE `user` DROP FOREIGN KEY FK_8D93D64992E6C00F');
        $this->addSql('DROP INDEX IDX_8D93D64992E6C00F ON `user`');
        $this->addSql('ALTER TABLE `user` DROP chapitre_guide_id');
    }
}
