<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220906093518 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE chapitre_guide CHANGE images images VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE guide ADD categorie_guide_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE guide ADD CONSTRAINT FK_CA9EC735EF6A5C21 FOREIGN KEY (categorie_guide_id) REFERENCES categorie_guide (id)');
        $this->addSql('CREATE INDEX IDX_CA9EC735EF6A5C21 ON guide (categorie_guide_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE chapitre_guide CHANGE images images LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\'');
        $this->addSql('ALTER TABLE guide DROP FOREIGN KEY FK_CA9EC735EF6A5C21');
        $this->addSql('DROP INDEX IDX_CA9EC735EF6A5C21 ON guide');
        $this->addSql('ALTER TABLE guide DROP categorie_guide_id');
    }
}
