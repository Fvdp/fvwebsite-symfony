/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
// start the Stimulus application
import './bootstrap';

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

// You can specify which plugins you need
import { Tooltip, Toast, Popover } from 'bootstrap';

// Fontawesome
import '/fontawesome/js/all.min.js';

// Loader
const loader = document.querySelector( '.loader' );
window.addEventListener( 'load', () =>
{
    loader.classList.add( 'fondu-out' );
} )


// agrandissement photo header
window.onload = () =>
{
    const modale = document.querySelector( "#modale-fv" );
    const close = document.querySelector( ".close" );
    const links = document.querySelectorAll( ".image-fv a" );

    for ( let link of links ) {
        link.addEventListener( "click", function ( e )
        {
            e.preventDefault();

            const image = modale_fv.querySelector( ".modal-fv-content img" );
            image.src = this.href;

            modale_fv.classList.add( "show" );
        } )
    }

    close.addEventListener( "click", function ()
    {
        modale_fv.classList.remove( "show" );
    } )

    modale_fv.addEventListener( "click", function ()
    {
        modale_fv.classList.remove( "show" );
    } )
}

// Ajout Enregistrement préférences utilisateur RGPD et Axeptio
void 0 === window._axcb && ( window._axcb = [] );
window._axcb.push( function ( axeptio )
{
    axeptio.on( "cookies:complete", function ( choices )
    {

    } )
} )

// effet transition sur page accueil sauf pour mobile
/* var media = window.matchMedia("(min-width: 991.98px)");
if (document.cookie.replace(/(?:(?:^|.*;s*)someCookieNames*=s*([^;]*).*$)|^.*$/, "$1") !== "true") {
    if (media.matches){
        document.getElementById('header').style.width='100%';
        setTimeout(function(){
            window.scrollTo(0, document.body.scrollHeight);
        },800)
        window.addEventListener('click', function header() {
            document.getElementById('header').style.width='280px';
        })
        setTimeout(function(){
            document.getElementById('header').style.width='280px';
        },2000);
        setTimeout(function(){
        document.documentElement.scrollTop = 0;
        },2400)
    }
    document.cookie = "someCookieName=true; path=/";
} */