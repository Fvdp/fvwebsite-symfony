# fvwebsite.com

fvwebsite est un portfolio

# Pré-requis

- php 8.1
- Composer
- Symfony CLI
- Docker
- Docker-compose
- Nodejs et npm

Vous pouvez vérifier les pré-requis (sauf docker et docker-compose) avec la commande suivante (de la CLI Symfony) :
symfony check:requirements

## Lancer l'environnement de développement

- composer install
- npm install
- npm run build
- docker-compose up -d
- symfony server -d

## Ajouter des données de tests

symfony console doctrine:fixture:load
