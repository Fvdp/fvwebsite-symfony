<?php

namespace App\Repository;

use App\Entity\Projets;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Projets>
 *
 * @method Projets|null find($id, $lockMode = null, $lockVersion = null)
 * @method Projets|null findOneBy(array $criteria, array $orderBy = null)
 * @method Projets[]    findAll()
 * @method Projets[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjetsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Projets::class);
    }

    public function add(Projets $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Projets $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
        *   Récupérer le type "web" des projets
     */
    public function last()
    {
        return $this->createQueryBuilder('b')
        ->where('b.type LIKE :type')
        ->andWhere('b.portfolio = TRUE')
        ->orderBy('b.dateRealisation', 'DESC')
        ->setParameter('type', 'web')
        ->setMaxResults(4)
        ->getQuery()
        ->getResult();
    }

    public function getTypeProjetWeb()
    {
        return $this->createQueryBuilder('p')
            ->where('p.type LIKE :type')
            ->orderBy('p.dateRealisation', 'DESC')
            ->setParameter('type', 'web')
            ->getQuery()
            ->getResult();
    }

    /**
        *   Récupérer le type "pao" des projets
     */
    public function getTypeProjetPao()
    {
        return $this->createQueryBuilder('p')
            ->where('p.type LIKE :type')
            ->andWhere('p.portfolio = TRUE')
            ->orderBy('p.dateRealisation', 'DESC')
            ->setParameter('type', 'pao')
            ->getQuery()
            ->getResult();
    }

    /**
        *   Récupérer le type "autre" des projets
     */
    public function getTypeProjetAutre()
    {
        return $this->createQueryBuilder('p')
            ->where('p.type LIKE :type')
            ->andWhere('p.portfolio = TRUE')
            ->orderBy('p.dateRealisation', 'DESC')
            ->setParameter('type', 'autre')
            ->getQuery()
            ->getResult();
    }


    /**
     * Récuperer détails projets
     **/
    public function findDetailsProjets(Projets $projets): array
    {
        return $this->createQueryBuilder('p')
        ->Where('p.portfolio = TRUE')
        ->setParameter('projets', $projets)
        ->getQuery()
        ->getResult();
    }

//    public function findOneBySomeField($value): ?Projets
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
