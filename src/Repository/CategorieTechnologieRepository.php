<?php

namespace App\Repository;

use App\Entity\CategorieTechnologie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CategorieTechnologie>
 *
 * @method CategorieTechnologie|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategorieTechnologie|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategorieTechnologie[]    findAll()
 * @method CategorieTechnologie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategorieTechnologieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CategorieTechnologie::class);
    }

    public function add(CategorieTechnologie $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(CategorieTechnologie $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
//    /**
//     * @return CategorieTechnologie[] Returns an array of CategorieTechnologie objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?CategorieTechnologie
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
