<?php

namespace App\Controller;

use App\Entity\CategorieTechnologie;
use App\Entity\Technologies;
use App\Repository\TechnologiesRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CategorieTechnologieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TechnologiesController extends AbstractController
{
    #[Route('/technologies', name: 'app_technologies')]
    public function technologiesIndex(
        CategorieTechnologieRepository $categorieTechnologieRepository,
        TechnologiesRepository $technologiesRepository,
        Technologies $technologies,
        CategorieTechnologie $categorieTechnologie
    ): Response {
        return $this->render('technologies/index.html.twig', [
            'categoriesTechRepo' => $categorieTechnologieRepository->findAll(),
            'techRepo' => $technologiesRepository->findAll(),
            'technologies' => $technologies,
            'categorieTech' => $categorieTechnologie,
        ]);
    }

    #[Route('/categories/technologies', name: 'app_categories_technologies')]
    public function categorieTechonogies(
        CategorieTechnologieRepository $categorieTechnologieRepository,
        TechnologiesRepository $technologiesRepository,
    ): Response {
        return $this->render('technologies/technologies.html.twig', [
            'categoriesTech' => $categorieTechnologieRepository->findAll(),
            'techRepo' => $technologiesRepository->findAll(),
        ]);
    }

    #[Route('/technologies/{slug}', name: 'app_technologie_details')]
    public function techonogiesDetails(
        Technologies $technologies,
    ): Response {
        return $this->render('technologies/details.html.twig', [
            'techno' => $technologies,
        ]);
    }
}
