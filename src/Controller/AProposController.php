<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AProposController extends AbstractController
{
    #[Route('/a-propos', name: 'app_a_propos')]
    public function aProposIndex(UserRepository $userRepository): Response
    {
        return $this->render('a_propos/a-propos.html.twig', [
            'user' => $userRepository->getUser(),
        ]);
    }
}
