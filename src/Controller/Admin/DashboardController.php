<?php

namespace App\Controller\Admin;

use App\Entity\Guide;
use App\Entity\Projets;
use App\Entity\EtapeGuide;
use App\Entity\Technologies;
use App\Entity\ChapitreGuide;
use App\Entity\CategorieTechnologie;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use Symfony\Component\Security\Core\User\UserInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig');

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureUserMenu(UserInterface $user): UserMenu
    {
        return parent::configureUserMenu($user)

        ->addMenuItems([
            MenuItem::linkToRoute('My Profile', 'fa fa-id-card', '...', ['...' => '...']),
            MenuItem::linkToRoute('Settings', 'fa fa-user-cog', '...', ['...' => '...']),
            MenuItem::section(),
            // MenuItem::linkToLogout('Logout', 'fa fa-sign-out'),
        ]);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Admin Fvwebsite');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToRoute('Aller sur le site', 'fa fa-house', 'app_home');

        yield MenuItem::section('');

        if ($this->isGranted('ROLE_ADMIN') && '...') {
            yield MenuItem::linkToCrud('Portfolio', 'fas fa-person-chalkboard', Projets::class);
            yield MenuItem::linkToCrud('CategorieTechnlogie', 'fas fa-person-chalkboard', CategorieTechnologie::class);
            yield MenuItem::linkToCrud('Technologies', 'fas fa-screwdriver-wrench', Technologies::class);

            yield MenuItem::section('Guides');
            if ($this->isGranted('ROLE_USER') && '...') {
                yield MenuItem::linkToUrl('Guides', 'fa fa-book', '/guide');
            }
            yield MenuItem::linkToCrud('Guide Projets', 'fas fa-book', Guide::class);
            yield MenuItem::linkToCrud('Thèmes', 'fas fa-book', ChapitreGuide::class);
            yield MenuItem::linkToCrud('Etapes', 'fas fa-book', EtapeGuide::class);

            yield MenuItem::section('Users');
            yield MenuItem::linkToCrud('Gestion utilisateurs', 'fas fa-person', User::class);
        }
    }
}
