<?php

namespace App\Controller\Admin;

use App\Entity\Projets;
use phpDocumentor\Reflection\Types\Boolean;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\FileUploadType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ProjetsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Projets::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('nom'),
            TextareaField::new('description')->hideOnIndex()
                ->setFormType(CKEditorType::class),
            DateField::new('dateRealisation'),
            ImageField::new('file')
            ->setBasePath('/uploads/portfolio/')
            ->setUploadDir('public/uploads/portfolio/')
            ->setFormType(FileUploadType::class)
            ->setRequired(false),
            // TextField::new('file')->hideOnIndex(),
            ImageField::new('images')->hideOnIndex()
                ->setFormTypeOptions([
                    "multiple" => true,
                    "attr" => [
                        "accept" => "image/png,image/pdf,image/jpeg"
                    ],
                ])
                ->setBasePath('/uploads/portfolio/')
                ->setUploadDir('public/uploads/portfolio/')
                ->setFormType(FileUploadType::class)
                ->setRequired(false),
            TextField::new('url')->hideOnIndex(),
            TextField::new('type'),
            BooleanField::new('portfolio'),
            BooleanField::new('enDev'),
            AssociationField::new('technologies'),
            SlugField::new('slug')->setTargetFieldName('nom')->hideOnIndex(),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
        ->setDefaultSort(['dateRealisation' => 'DESC']);
    }
}
