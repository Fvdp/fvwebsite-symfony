<?php

namespace App\Controller\Admin;

use App\Entity\CategorieTechnologie;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class CategorieTechnologieCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return CategorieTechnologie::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('nom'),
            TextField::new('description'),
            TextField::new('file'),
            AssociationField::new('technologies'),
            SlugField::new('slug')->setTargetFieldName('nom')->hideOnIndex(),
        ];
    }
}
