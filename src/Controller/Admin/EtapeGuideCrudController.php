<?php

namespace App\Controller\Admin;

use App\Entity\EtapeGuide;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\FileUploadType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class EtapeGuideCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return EtapeGuide::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('nom'),
            TextareaField::new('description')
                ->setFormType(CKEditorType::class),
            AssociationField::new('chapitreGuide'),
            ImageField::new('images')
                ->setBasePath('/uploads/guides/')
                ->setUploadDir('public/uploads/guides/')
                ->setFormType(FileUploadType::class)
                ->setRequired(false),
            SlugField::new('slug')->setTargetFieldName('nom')->hideOnIndex(),
        ];
    }
}
