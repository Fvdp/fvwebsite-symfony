<?php

namespace App\Controller\Admin;

use App\Entity\Guide;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\FileUploadType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class GuideCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Guide::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig');
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('nom'),
            TextareaField::new('description'),
                // ->setFormType(CKEditorType::class),
            AssociationField::new('categorieGuide'),
            AssociationField::new('projet'),
            ImageField::new('file')
                ->setBasePath('/uploads/guides/')
                ->setUploadDir('public/uploads/guides/')
                ->setFormType(FileUploadType::class)
                ->setRequired(false),
            // TextField::new('file')->hideOnIndex(),
            // ImageField::new('images')
            //     ->setFormTypeOptions([
            //         "multiple" => true,
            //         "attr" => [
            //             "accept" => "image/png,image/pdf,image/jpeg"
            //         ],
            //     ])
            //     ->setBasePath('/uploads/guides/')
            //     ->setUploadDir('public/uploads/guides/')
            //     ->setFormType(FileUploadType::class)
            //     ->setRequired(false),
            SlugField::new('slug')->setTargetFieldName('nom')->hideOnIndex(),
        ];
    }
}
