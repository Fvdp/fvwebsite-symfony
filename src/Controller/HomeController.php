<?php

namespace App\Controller;

use App\Entity\CategorieTechnologie;
use App\Entity\Projets;
use App\Repository\UserRepository;
use App\Repository\ProjetsRepository;
use App\Repository\TechnologiesRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CategorieTechnologieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function indexBienvenue(
        CategorieTechnologieRepository $categorieTechnologieRepository,
        TechnologiesRepository $technologiesRepository,
        UserRepository $userRepository,
        ProjetsRepository $projetsRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $data = $projetsRepository->last();

        $portfolioWeb = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            6
        );
        return $this->render('home/index.html.twig', [
            'categoriesTech' => $categorieTechnologieRepository->findAll(),
            'techRepo' => $technologiesRepository->findAll(),
            'user' => $userRepository->getUser(),
            'portfolioIndex' => $portfolioWeb,
        ]);
    }
}
