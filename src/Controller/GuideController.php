<?php

namespace App\Controller;

use App\Entity\ChapitreGuide;
use App\Entity\CategorieGuide;
use App\Entity\Guide;
use App\Repository\GuideRepository;
use App\Repository\CategorieGuideRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class GuideController extends AbstractController
{
    #[Route('/guide', name: 'app_guide')]
    public function index(
        GuideRepository $guideRepository,
        CategorieGuideRepository $categorieGuideRepository,
    ): Response {
        return $this->render('guide/index.html.twig', [
            'categorieGuide' => $categorieGuideRepository->findAll(),
            'guide' => $guideRepository->findAll(),
        ]);
    }

    /* Afficher Theme guide */
    #[Route('/guide/theme/{slug}', name: 'guide_theme')]
    /**
    * @IsGranted("ROLE_USER")
    */
    public function guideTheme(
        ChapitreGuide $chapitreGuide,
    ): Response {
        return $this->render('guide/guide-theme.html.twig', [
            'theme' => $chapitreGuide,
        ]);
    }

    /* Afficher projets guide */
    #[Route('/guide/{slug}', name: 'guide')]
    /**
    * @IsGranted("ROLE_USER")
    */
    public function guide(
        GuideRepository $guideRepository,
    ): Response {
        return $this->render('guide/guide-projet.html.twig', [
            'guide' => $guideRepository->findAll(),
        ]);
    }
}
