<?php

namespace App\Controller;

use App\Entity\Projets;
use App\Entity\Technologies;
use App\Repository\ProjetsRepository;
use App\Repository\TechnologiesRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class PortfolioController extends AbstractController
{
    #[Route('/portfolio/web', name: 'app_portfolio_web')]
    public function web(
        ProjetsRepository $projetsRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $dataWeb = $projetsRepository->getTypeProjetWeb();

        $portfolioWeb = $paginator->paginate(
            $dataWeb,
            $request->query->getInt('page', 1),
            6
        );
        return $this->render('portfolio/portfolio-web.html.twig', [
            'portfolioWeb' => $portfolioWeb,
        ]);
    }

    #[Route('/portfolio/pao', name: 'app_portfolio_pao')]
    public function pao(
        ProjetsRepository $projetsRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $dataPao = $projetsRepository->getTypeProjetPao();

        $portfolioPao = $paginator->paginate(
            $dataPao,
            $request->query->getInt('page', 1),
            10
        );
        return $this->render('portfolio/portfolio-pao.html.twig', [
            'portfolioPao' => $portfolioPao,
        ]);
    }

    #[Route('/portfolio/autre', name: 'app_portfolio_autre')]
    public function autre(
        ProjetsRepository $projetsRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $dataAutre = $projetsRepository->getTypeProjetAutre();

        $portfolioAutre = $paginator->paginate(
            $dataAutre,
            $request->query->getInt('page', 1),
            4
        );

        return $this->render('portfolio/portfolio-autre.html.twig', [
            'portfolioAutre' => $portfolioAutre,
        ]);
    }

    /* Afficher détails de chaque projet */
    #[Route('/portfolio/{slug}', name: 'portfolio_details')]
    public function detailsProjet(
        ProjetsRepository $projetsRepository,
        TechnologiesRepository $technologiesRepository,
        Projets $projets,
    ): Response {
        return $this->render('portfolio/portfolio-details.html.twig', [
            'projetRepo' => $projetsRepository->findAll(),
            'technologies' => $technologiesRepository->findAll(),
            'projet' => $projets,
        ]);
    }
}
