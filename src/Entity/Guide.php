<?php

namespace App\Entity;

use App\Repository\GuideRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GuideRepository::class)]
class Guide
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $nom;

    #[ORM\Column(type: 'text')]
    private $description;

    #[ORM\Column(type: 'string', length: 255)]
    private $file;

    #[ORM\Column(type: 'string', length: 255)]
    private $slug;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'guideWiki')]
    private $utilisateur;

    #[ORM\ManyToMany(targetEntity: Projets::class, inversedBy: 'guides')]
    private $projet;

    #[ORM\ManyToMany(targetEntity: CategorieGuide::class, inversedBy: 'guides')]
    private $categorieGuideWiki;

    #[ORM\Column(type: Types::ARRAY, nullable: true)]
    private $images = [];

    #[ORM\OneToMany(mappedBy: 'guide', targetEntity: ChapitreGuide::class)]
    private Collection $chapitreGuides;

    #[ORM\ManyToOne(inversedBy: 'guide')]
    private ?categorieGuide $categorieGuide = null;

    public function __construct()
    {
        $this->projet = new ArrayCollection();
        $this->categorieGuideWiki = new ArrayCollection();
        $this->chapitreGuides = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getUtilisateur(): ?User
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?User $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * @return Collection<int, Projets>
     */
    public function getProjet(): Collection
    {
        return $this->projet;
    }

    public function addProjet(Projets $projet): self
    {
        if (!$this->projet->contains($projet)) {
            $this->projet[] = $projet;
        }

        return $this;
    }

    public function removeProjet(Projets $projet): self
    {
        $this->projet->removeElement($projet);

        return $this;
    }

    /**
     * @return Collection<int, CategorieGuide>
     */
    public function getCategorieGuideWiki(): Collection
    {
        return $this->categorieGuideWiki;
    }

    public function addCategorieGuideWiki(CategorieGuide $categorieGuideWiki): self
    {
        if (!$this->categorieGuideWiki->contains($categorieGuideWiki)) {
            $this->categorieGuideWiki[] = $categorieGuideWiki;
        }

        return $this;
    }

    public function removeCategorieGuideWiki(CategorieGuide $categorieGuideWiki): self
    {
        $this->categorieGuideWiki->removeElement($categorieGuideWiki);

        return $this;
    }

    public function getImages(): array
    {
        return $this->images;
    }

    public function setImages(?array $images): self
    {
        $this->images = $images;

        return $this;
    }

    /**
     * @return Collection<int, ChapitreGuide>
     */
    public function getChapitreGuides(): Collection
    {
        return $this->chapitreGuides;
    }

    public function addChapitreGuide(ChapitreGuide $chapitreGuide): self
    {
        if (!$this->chapitreGuides->contains($chapitreGuide)) {
            $this->chapitreGuides->add($chapitreGuide);
            $chapitreGuide->setGuide($this);
        }

        return $this;
    }

    public function removeChapitreGuide(ChapitreGuide $chapitreGuide): self
    {
        if ($this->chapitreGuides->removeElement($chapitreGuide)) {
            // set the owning side to null (unless already changed)
            if ($chapitreGuide->getGuide() === $this) {
                $chapitreGuide->setGuide(null);
            }
        }

        return $this;
    }

    public function getCategorieGuide(): ?categorieGuide
    {
        return $this->categorieGuide;
    }

    public function setCategorieGuide(?categorieGuide $categorieGuide): self
    {
        $this->categorieGuide = $categorieGuide;

        return $this;
    }

    public function __toString()
    {
        return $this->nom;
    }
}
