<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[UniqueEntity(fields: ['email'], message: 'There is already an account with this email')]
#[UniqueEntity(fields: ['email'], message: 'There is already an account with this email')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    private $email;

    #[ORM\Column(type: 'json')]
    private $roles = [];

    #[ORM\Column(type: 'string')]
    private $password;

    #[ORM\Column(type: 'string', length: 255)]
    private $prenom;

    #[ORM\Column(type: 'string', length: 255)]
    private $nom;

    #[ORM\Column(type: 'text')]
    private $aPropos;

    #[ORM\OneToMany(mappedBy: 'utilisateur', targetEntity: Projets::class)]
    private $projet;

    #[ORM\OneToMany(mappedBy: 'utilisateur', targetEntity: CategorieGuide::class)]
    private $categorieGuideWiki;

    #[ORM\OneToMany(mappedBy: 'utilisateur', targetEntity: Guide::class)]
    private $guideWiki;

    #[ORM\Column(type: 'boolean')]
    private $isVerified = false;

    #[ORM\Column(type: 'datetime_immutable', options: ['default' => 'CURRENT_TIMESTAMP'])]
    private $createdAt;

    #[ORM\ManyToOne(inversedBy: 'user')]
    private ?ChapitreGuide $chapitreGuide = null;

    public function __construct()
    {
        $this->projet = new ArrayCollection();
        $this->categorieGuideWiki = new ArrayCollection();
        $this->guideWiki = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = "ROLE_USER";

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAPropos(): ?string
    {
        return $this->aPropos;
    }

    public function setAPropos(string $aPropos): self
    {
        $this->aPropos = $aPropos;

        return $this;
    }

    /**
     * @return Collection<int, Projets>
     */
    public function getProjet(): Collection
    {
        return $this->projet;
    }

    public function addProjet(Projets $projet): self
    {
        if (!$this->projet->contains($projet)) {
            $this->projet[] = $projet;
            $projet->setUtilisateur($this);
        }

        return $this;
    }

    public function removeProjet(Projets $projet): self
    {
        if ($this->projet->removeElement($projet)) {
            // set the owning side to null (unless already changed)
            if ($projet->getUtilisateur() === $this) {
                $projet->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, CategorieGuide>
     */
    public function getCategorieGuideWiki(): Collection
    {
        return $this->categorieGuideWiki;
    }

    public function addCategorieGuideWiki(CategorieGuide $categorieGuideWiki): self
    {
        if (!$this->categorieGuideWiki->contains($categorieGuideWiki)) {
            $this->categorieGuideWiki[] = $categorieGuideWiki;
            $categorieGuideWiki->setUtilisateur($this);
        }

        return $this;
    }

    public function removeCategorieGuideWiki(CategorieGuide $categorieGuideWiki): self
    {
        if ($this->categorieGuideWiki->removeElement($categorieGuideWiki)) {
            // set the owning side to null (unless already changed)
            if ($categorieGuideWiki->getUtilisateur() === $this) {
                $categorieGuideWiki->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Guide>
     */
    public function getGuideWiki(): Collection
    {
        return $this->guideWiki;
    }

    public function addGuideWiki(Guide $guideWiki): self
    {
        if (!$this->guideWiki->contains($guideWiki)) {
            $this->guideWiki[] = $guideWiki;
            $guideWiki->setUtilisateur($this);
        }

        return $this;
    }

    public function removeGuideWiki(Guide $guideWiki): self
    {
        if ($this->guideWiki->removeElement($guideWiki)) {
            // set the owning side to null (unless already changed)
            if ($guideWiki->getUtilisateur() === $this) {
                $guideWiki->setUtilisateur(null);
            }
        }

        return $this;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getChapitreGuide(): ?ChapitreGuide
    {
        return $this->chapitreGuide;
    }

    public function setChapitreGuide(?ChapitreGuide $chapitreGuide): self
    {
        $this->chapitreGuide = $chapitreGuide;

        return $this;
    }
}
