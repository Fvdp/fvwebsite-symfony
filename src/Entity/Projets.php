<?php

namespace App\Entity;

use App\Repository\ProjetsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProjetsRepository::class)]
class Projets
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $nom;

    #[ORM\Column(type: 'datetime')]
    private $dateRealisation;

    #[ORM\Column(type: 'datetime')]
    private $date;

    #[ORM\Column(type: 'text')]
    private $description;

    #[ORM\Column(type: 'boolean')]
    private $portfolio;

    #[ORM\Column(type: 'boolean')]
    private $enDev;

    #[ORM\Column(type: 'string', length: 255)]
    private $file;

    #[ORM\Column(type: 'string', length: 255)]
    private $slug;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'projet')]
    private $utilisateur;

    #[ORM\ManyToMany(targetEntity: Technologies::class, mappedBy: 'projet')]
    private $technologies;

    #[ORM\ManyToMany(targetEntity: CategorieTechnologie::class, mappedBy: 'projet')]
    private $categorieTechnologies;

    #[ORM\ManyToMany(targetEntity: Temoignages::class, mappedBy: 'projet')]
    private $temoignages;

    #[ORM\ManyToMany(targetEntity: Guide::class, mappedBy: 'projet')]
    private $guides;

    #[ORM\Column(type: 'string', length: 255)]
    private $type;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $url = null;

    #[ORM\Column(type: Types::ARRAY, nullable: true)]
    private array $images = [];

    public function __construct()
    {
        $this->technologies = new ArrayCollection();
        $this->categorieTechnologies = new ArrayCollection();
        $this->temoignages = new ArrayCollection();
        $this->guides = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDateRealisation(): ?\DateTimeInterface
    {
        return $this->dateRealisation;
    }

    public function setDateRealisation(\DateTimeInterface $dateRealisation): self
    {
        $this->dateRealisation = $dateRealisation;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function isPortfolio(): ?bool
    {
        return $this->portfolio;
    }

    public function setPortfolio(bool $portfolio): self
    {
        $this->portfolio = $portfolio;

        return $this;
    }

    public function isEnDev(): ?bool
    {
        return $this->enDev;
    }

    public function setEnDev(bool $enDev): self
    {
        $this->enDev = $enDev;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getUtilisateur(): ?User
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?User $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * @return Collection<int, Technologies>
     */
    public function getTechnologies(): Collection
    {
        return $this->technologies;
    }

    public function addTechnology(Technologies $technology): self
    {
        if (!$this->technologies->contains($technology)) {
            $this->technologies[] = $technology;
            $technology->addProjet($this);
        }

        return $this;
    }

    public function removeTechnology(Technologies $technology): self
    {
        if ($this->technologies->removeElement($technology)) {
            $technology->removeProjet($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, CategorieTechnologie>
     */
    public function getCategorieTechnologies(): Collection
    {
        return $this->categorieTechnologies;
    }

    public function addCategorieTechnology(CategorieTechnologie $categorieTechnology): self
    {
        if (!$this->categorieTechnologies->contains($categorieTechnology)) {
            $this->categorieTechnologies[] = $categorieTechnology;
            $categorieTechnology->addProjet($this);
        }

        return $this;
    }

    public function removeCategorieTechnology(CategorieTechnologie $categorieTechnology): self
    {
        if ($this->categorieTechnologies->removeElement($categorieTechnology)) {
            $categorieTechnology->removeProjet($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Temoignages>
     */
    public function getTemoignages(): Collection
    {
        return $this->temoignages;
    }

    public function addTemoignage(Temoignages $temoignage): self
    {
        if (!$this->temoignages->contains($temoignage)) {
            $this->temoignages[] = $temoignage;
            $temoignage->addProjet($this);
        }

        return $this;
    }

    public function removeTemoignage(Temoignages $temoignage): self
    {
        if ($this->temoignages->removeElement($temoignage)) {
            $temoignage->removeProjet($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Guide>
     */
    public function getGuides(): Collection
    {
        return $this->guides;
    }

    public function addGuide(Guide $guide): self
    {
        if (!$this->guides->contains($guide)) {
            $this->guides[] = $guide;
            $guide->addProjet($this);
        }

        return $this;
    }

    public function removeGuide(Guide $guide): self
    {
        if ($this->guides->removeElement($guide)) {
            $guide->removeProjet($this);
        }

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getImages(): array
    {
        return $this->images;
    }

    public function setImages(?array $images): self
    {
        $this->images = $images;

        return $this;
    }

    public function __toString()
    {
        return $this->nom;
    }
}
