<?php

namespace App\Entity;

use App\Repository\TechnologiesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TechnologiesRepository::class)]
class Technologies
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $nom;

    #[ORM\Column(type: 'text')]
    private $description;

    #[ORM\Column(type: 'string', length: 255)]
    private $file;

    #[ORM\Column(type: 'string', length: 255)]
    private $slug;

    #[ORM\ManyToMany(targetEntity: Projets::class, inversedBy: 'technologies')]
    private $projet;

    #[ORM\ManyToMany(targetEntity: CategorieTechnologie::class, inversedBy: 'technologies')]
    private $categorieTechno;

    public function __construct()
    {
        $this->projet = new ArrayCollection();
        $this->categorieTechno = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection<int, Projets>
     */
    public function getProjet(): Collection
    {
        return $this->projet;
    }

    public function addProjet(Projets $projet): self
    {
        if (!$this->projet->contains($projet)) {
            $this->projet[] = $projet;
        }

        return $this;
    }

    public function removeProjet(Projets $projet): self
    {
        $this->projet->removeElement($projet);

        return $this;
    }

    /**
     * @return Collection<int, CategorieTechnologie>
     */
    public function getCategorieTechno(): Collection
    {
        return $this->categorieTechno;
    }

    public function addCategorieTechno(CategorieTechnologie $categorieTechno): self
    {
        if (!$this->categorieTechno->contains($categorieTechno)) {
            $this->categorieTechno[] = $categorieTechno;
        }

        return $this;
    }

    public function removeCategorieTechno(CategorieTechnologie $categorieTechno): self
    {
        $this->categorieTechno->removeElement($categorieTechno);

        return $this;
    }

    public function __toString()
    {
        return $this->nom;
    }
}
