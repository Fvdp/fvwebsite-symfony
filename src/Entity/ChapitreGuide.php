<?php

namespace App\Entity;

use App\Repository\ChapitreGuideRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ChapitreGuideRepository::class)]
class ChapitreGuide
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(type: 'string', length: 255)]
    private $images;

    #[ORM\Column(length: 255)]
    private ?string $slug = null;

    #[ORM\OneToMany(mappedBy: 'chapitreGuide', targetEntity: User::class)]
    private Collection $user;

    #[ORM\ManyToOne(inversedBy: 'chapitreGuides')]
    #[ORM\JoinColumn(nullable: false)]
    private ?guide $guide = null;

    #[ORM\OneToMany(mappedBy: 'chapitreGuide', targetEntity: EtapeGuide::class)]
    private Collection $etapeGuides;

    public function __construct()
    {
        $this->user = new ArrayCollection();
        $this->etapeGuides = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImages(): ?string
    {
        return $this->images;
    }

    public function setImages(string $images): self
    {
        $this->images = $images;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection<int, user>
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(user $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user->add($user);
            $user->setChapitreGuide($this);
        }

        return $this;
    }

    public function removeUser(user $user): self
    {
        if ($this->user->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getChapitreGuide() === $this) {
                $user->setChapitreGuide(null);
            }
        }

        return $this;
    }

    public function getGuide(): ?guide
    {
        return $this->guide;
    }

    public function setGuide(?guide $guide): self
    {
        $this->guide = $guide;

        return $this;
    }

    /**
     * @return Collection<int, EtapeGuide>
     */
    public function getEtapeGuides(): Collection
    {
        return $this->etapeGuides;
    }

    public function addEtapeGuide(EtapeGuide $etapeGuide): self
    {
        if (!$this->etapeGuides->contains($etapeGuide)) {
            $this->etapeGuides->add($etapeGuide);
            $etapeGuide->setChapitreGuide($this);
        }

        return $this;
    }

    public function removeEtapeGuide(EtapeGuide $etapeGuide): self
    {
        if ($this->etapeGuides->removeElement($etapeGuide)) {
            // set the owning side to null (unless already changed)
            if ($etapeGuide->getChapitreGuide() === $this) {
                $etapeGuide->setChapitreGuide(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->nom;
    }
}
