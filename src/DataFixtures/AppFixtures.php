<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use App\Entity\Guide;
use App\Entity\Projets;
use App\Entity\Technologies;
use Doctrine\ORM\Mapping\Id;
use App\Entity\CategorieGuide;
use App\Entity\CategorieTechnologie;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // use the factory to create a Faker
        $faker = Factory::create('fr_FR');

        // Create user
        $user = new User();

        $user->setEmail('user@test.com')
        ->setPrenom($faker->firstname())
        ->setNom($faker->lastname())
        ->setAPropos($faker->text())
        ->setRoles(['ROLE_USER'])
        ->setPassword('$2y$13$yHAHmVyi1.W1FxEOGFCtbeO5KJRXPk7h7nTbNyfsRUDQWofdavV2G');

        $manager->persist($user);

        // Create user Admin fvwebsite
        $user = new User();

        $user->setEmail('contact@fvwebsite.com')
        ->setPrenom('Frédéric')
        ->setNom('Vandenplas')
        ->setAPropos('Passionné de programmation web, 
            j\'ai suivi une formation avec Aliptic, 
            Association d\'entrepreneurs du numérique à Limoges.')
        ->setRoles(['ROLE_ADMIN'])
        ->setPassword('$2y$13$yHAHmVyi1.W1FxEOGFCtbeO5KJRXPk7h7nTbNyfsRUDQWofdavV2G');

        $manager->persist($user);

        // création de 5 CategorieTechonologie
        $catTechno1 = new CategorieTechnologie();
        $catTechno1->setNom('Frontend')
        ->setDescription('Partie visible de l\'application, conçu pour être manipulé par l\'utilisateur')
        ->setFile('fa-solid fa-chalkboard-user')
        ->setSlug($faker->slug(1));
        $manager->persist($catTechno1);

        $catTechno2 = new CategorieTechnologie();
        $catTechno2->setNom('Backend')
        ->setDescription(
            'Partie invisible de l\'application, 
            fonctionne en arrière plan pour réaliser les actions du site'
        )
        ->setFile('fa-solid fa-code')
        ->setSlug($faker->slug(1));
        $manager->persist($catTechno2);

        $catTechno3 = new CategorieTechnologie();
        $catTechno3->setNom('CMS')
        ->setDescription('Système de gestion de contenu : Wordpress, Prestashop, Dolibarr ...')
        ->setFile('fa-brands fa-wordpress')
        ->setSlug($faker->slug(1));
        $manager->persist($catTechno3);

        $catTechno4 = new CategorieTechnologie();
        $catTechno4->setNom('Conception graphique')
        ->setDescription('Création graphique et outils d\'aide à la conception graphique.')
        ->setFile('fa-solid fa-photo-film')
        ->setSlug($faker->slug(1));
        $manager->persist($catTechno4);

        $catTechno5 = new CategorieTechnologie();
        $catTechno5->setNom('Serveur - Hébergement')
        ->setDescription('Système qui permet de stocker, diffuser des contenus internet ou intranet.')
        ->setFile('fa-solid fa-server')
        ->setSlug($faker->slug(1));
        $manager->persist($catTechno5);

        $catTechno6 = new CategorieTechnologie();
        $catTechno6->setNom('Outils collaboratifs')
        ->setDescription('Outils utilent à la conception/réalisation d\'un projet web.')
        ->setFile('fa-solid fa-code-branch')
        ->setSlug($faker->slug(1));
        $manager->persist($catTechno6);

        // création Techonologies
        $Techno1 = new Technologies();
        $Techno1->setNom('html')
        ->setDescription(
            'HyperText Markup Language (HTML) est le code utilisé 
            pour structurer une page web et son contenu.'
        )
        ->setFile('fa-brands fa-html5')
        ->setSlug($faker->slug(1))
        ->addCategorieTechno($catTechno1);
        $manager->persist($Techno1);

        $Techno2 = new Technologies();
        $Techno2->setNom('css')
        ->setDescription(
            'Cascading Style Sheets en anglais, ou feuilles de style en cascade. 
            Le Css est utilisé pour mettre en forme une page web.'
        )
        ->setFile('fa-brands fa-css3')
        ->setSlug($faker->slug(1))
        ->addCategorieTechno($catTechno1);
        $manager->persist($Techno2);

        $Techno3 = new Technologies();
        $Techno3->setNom('PHP - MySQL')
        ->setDescription(
            'Le language PHP et plus particulièrement le couple PHP-MySql sont très utilisés pour 
            le développement Web. 
            La majorité des développeurs web utile ces systèmes pour créer des sites web dynamiques.
            Ce sont des technologies Open-Source qui sont très suivies par une large communauté de 
            contributeurs. 
            PHP fait partie des 8 languages de programmation les plus utilisés, c\'est un language 
            de script exécuté côté serveur. 
            MySQL est un système de gestion de base de données relationnel, il utilise des requêtes 
            structurées (SQL)'
        )
        ->setFile('fa-brands fa-php')
        ->setSlug($faker->slug(1))
        ->addCategorieTechno($catTechno2);
        $manager->persist($Techno3);

        $Techno4 = new Technologies();
        $Techno4->setNom('Symfony')
        ->setDescription(
            'Le framewordk Symfony fournit des fonctionnalités modulables et adaptables pour 
            accélérer le développement d\'un site web en language PHP. Il optimise les performances et 
            la sécurité des applications web'
        )
        ->setFile('fa-brands fa-symfony')
        ->setSlug($faker->slug(1))
        ->addCategorieTechno($catTechno2);
        $manager->persist($Techno4);

        // création de 10 projets web
        for ($i = 0; $i < 10; $i++) {
            $projet = new Projets();

            $projet->setUtilisateur($user)
            ->setNom($faker->words(1, true))
            ->setDateRealisation($faker->dateTimeBetween('-6 month', 'now'))
            ->setDate($faker->dateTimeBetween('-6 month', 'now'))
            ->setDescription($faker->text(350))
            ->setPortfolio($faker->randomElement([true, false]))
            ->setEnDev($faker->randomElement([true, false]))
            ->setFile('/img/web/comptesens-1.jpg')
            ->setSlug($faker->slug(1))
            ->setType('web');

            $manager->persist($projet);
        }

        // création de 6 projets pao
        for ($i = 0; $i < 6; $i++) {
            $projet = new Projets();

            $projet->setUtilisateur($user)
            ->setNom($faker->words(1, true))
            ->setDateRealisation($faker->dateTimeBetween('-6 month', 'now'))
            ->setDate($faker->dateTimeBetween('-6 month', 'now'))
            ->setDescription($faker->text(350))
            ->setPortfolio($faker->randomElement([true, false]))
            ->setEnDev($faker->randomElement([true, false]))
            ->setFile('https://placeimg.com/600/300/nature')
            ->setSlug($faker->slug(1))
            ->setType('pao');

            $manager->persist($projet);
        }

        // création de 3 projets autres
        for ($i = 0; $i < 3; $i++) {
            $projet = new Projets();

            $projet->setUtilisateur($user)
            ->setNom($faker->words(1, true))
            ->setDateRealisation($faker->dateTimeBetween('-6 month', 'now'))
            ->setDate($faker->dateTimeBetween('-6 month', 'now'))
            ->setDescription($faker->text(350))
            ->setPortfolio($faker->randomElement([true, false]))
            ->setEnDev($faker->randomElement([true, false]))
            ->setFile('https://placeimg.com/600/300/nature')
            ->setSlug($faker->slug(1))
            ->setType('autre');

            $manager->persist($projet);
        }

        // création de 3 Categories Guides
        $catGuide1 = new CategorieGuide();
        $catGuide1->setUtilisateur($user)
        ->setNom('Projet')
        ->setDescription('Guides selon projet')
        ->setFile('fa-solid fa-list-check')
        ->setSlug('categorie-projet');
        $manager->persist($catGuide1);

        $catGuide2 = new CategorieGuide();
        $catGuide2->setUtilisateur($user)
        ->setNom('Dolibarr')
        ->setDescription('Guides pour le logiciel Dolibarr GRC')
        ->setFile('fa-solid fa-cash-register')
        ->setSlug('categorie-dolibarr');
        $manager->persist($catGuide2);

        $catGuide3 = new CategorieGuide();
        $catGuide3->setUtilisateur($user)
        ->setNom('Prestashop')
        ->setDescription('Guides pour le CMS Prestashop - boutique en ligne')
        ->setFile('fa-solid fa-basket-shopping')
        ->setSlug('categorie-prestashop');
        $manager->persist($catGuide3);

        // création de 10 Guides
        for ($i = 0; $i < 10; $i++) {
            $catGuide = new Guide();

            $catGuide->setUtilisateur($user)
            ->setNom($faker->word(3, true))
            ->setDescription($faker->text())
            ->setFile('https://placeimg.com/600/300/nature')
            ->setSlug($faker->slug(1));

            $manager->persist($catGuide);
        }

        $manager->flush();
    }
}
